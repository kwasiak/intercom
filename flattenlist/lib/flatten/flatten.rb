module Flatten
  class Flatten
    def flat_array(array)

      #initial check for nil, empty string and array input
      return array if array.nil? || array == [] || !array.kind_of?(Array)

      output = []

      array.each do |element|

        # if element is array, then make a recurrent call,
        # else add element to the output
        output = element.kind_of?(Array) ? output | flat_array(element) : output << element
      end

      output
    end
  end
end
