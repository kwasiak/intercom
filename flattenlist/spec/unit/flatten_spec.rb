require 'rspec'
require 'flatten/flatten'

describe 'Flatten', :unit do

  it 'returns nil for entry nil' do
    flatten = create_flatten

    expect(flatten.flat_array(nil)).to eq(nil)
  end

  it 'returns empty list for empty list' do
    flatten = create_flatten

    expect(flatten.flat_array([])).to eq([])
  end

  it 'returns input if input is not an array' do
    flatten = create_flatten

    expect(flatten.flat_array(244)).to eq(244)
  end

  it 'returns flat list for flat list' do
    flatten = create_flatten

    expect(flatten.flat_array([1,5,10,16,18])).to eq ([1,5,10,16,18])
  end

  it 'returns flat list for 2 level list' do
    flatten = create_flatten

    expect(flatten.flat_array([1,5,[10,16],[18]])).to eq ([1,5,10,16,18])
  end

  it 'returns flat list for n-level list' do
    flatten = create_flatten

    expect(flatten.flat_array([1,2,[11,12,13,[21,22],14],3,4])).to eq ([1,2,11,12,13,21,22,14,3,4])
  end

  private

  def create_flatten
    Flatten::Flatten.new
  end

end