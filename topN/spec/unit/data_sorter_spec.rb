require 'rspec'
require 'sorter/data_sorter'

describe 'DataSorter', :unit do

  it 'fails initialization of number of elements is 0 or less' do
    expect {DataSorter::DataSorter.new(0)}.to raise_error
    expect {DataSorter::DataSorter.new(-2)}.to raise_error
  end

  it 'fails when insert nil' do
    sorter = create_sorter(5)
    expect {sorter.insert_new_data(nil)}.to raise_error
  end

  it 'fails when insert no array' do
    sorter = create_sorter(6)
    expect {sorter.insert_new_data('no array')}.to raise_error
  end

  it 'returns only nils when no data inserted' do
    sorter = create_sorter(4)
    expect(sorter.highest_numbers).to eq([nil, nil, nil, nil])
  end

  it 'returns nils if there''s not enough data' do
    sorter = create_sorter(4)
    sorter.insert_new_data([1,2.34])
    expect(sorter.highest_numbers).to eq([2.34, 1, nil, nil])
  end

  it 'returns list of highest numbers from passed collection for single insert' do
    sorter = create_sorter(3)
    sorter.insert_new_data([-2.3, 0,0, 1, -10.6, -3.4])
    expect(sorter.highest_numbers).to eq([1,0,0])
  end

  it 'returns list of highest numbers from passed collection for single 3 inserts' do
    sorter = create_sorter(3)
    sorter.insert_new_data([-2.3, 0,0])
    sorter.insert_new_data([-1])
    sorter.insert_new_data([-5,-6,-7])
    expect(sorter.highest_numbers).to eq([0,0,-1])
  end

  ### is_new_number_larger? specs
  it 'takes nil as smaller than any number' do
    sorter = create_sorter(4)
    expect(sorter.instance_eval{ is_new_number_larger?(nil, 6) }).to eq(true)
  end

  it 'compares larger with smaller' do
    sorter = create_sorter(4)

    expect(sorter.instance_eval{ is_new_number_larger?(5.2, 6) }).to eq(true)
  end

  it 'compares smaller with larger' do
    sorter = create_sorter(4)
    expect(sorter.instance_eval{ is_new_number_larger?(8.5, 5.2) }).to eq(false)
  end

  def create_sorter(numbers)
    DataSorter::DataSorter.new(numbers)
  end

end