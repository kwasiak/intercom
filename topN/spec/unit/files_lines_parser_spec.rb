require 'rspec'
require 'file/file_lines_parser'

describe 'FileLineParser', :unit do

  it 'fails on nil line' do
    expect { create_file_line_parser.parse_file_line(nil) }.to raise_error
  end

  it 'fails if input line is not string' do
    expect { create_file_line_parser.parse_file_line([1,2]) }.to raise_error
  end

  it 'returns empty array if line is empty string' do
    expect(create_file_line_parser.parse_file_line('')).to eq([])
  end

  it 'returns array of numbers (real and integer) on correct input' do
    expect(create_file_line_parser.parse_file_line('5|3.65|-0.9'))
        .to eq([5, 3.65, -0.9])
  end

  it 'fails if one of the items is not a number' do
    expect { create_file_line_parser.parse_file_line('5|3.65bla|-0.9') }
        .to raise_error
  end

  private

  def create_file_line_parser
    FileParser::FileLineParser.new
  end
end