module DataSorter
  class DataSorter

    attr_reader :number_of_highest
    attr_reader :highest_numbers

    def initialize(number_of_highest)
      raise 'number_of_highest must be larger than 0' if number_of_highest <= 0

      @number_of_highest = number_of_highest
      @highest_numbers = Array.new(number_of_highest)
    end

    def insert_new_data(data)

      raise 'input data must be not empty' if data.nil? || !data.kind_of?(Array)

      data.each do |number|
        insert_number(number)
      end
    end

    private

    def insert_number(number)
      for index in 0 ... highest_numbers.size
        if is_new_number_larger?(highest_numbers[index], number)
          highest_numbers.insert(index, number)

          # remove last element
          highest_numbers.delete_at(-1)

          break
        end
      end
    end

    def is_new_number_larger?(reference, new_number)
      reference.nil? || reference < new_number
    end
  end
end