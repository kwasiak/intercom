require 'sorter/data_sorter'
require 'file/file_lines_parser'

module FileParser
  class FileTopNNumbers

    def find_topn_numbers_in_file(filepath, numberscount)

      raise "File #{filepath} doesn't exist" unless File.exist?(filepath)

      sorter = DataSorter::DataSorter.new(numberscount)
      lineparser = FileParser::FileLineParser.new

      begin
        File.readlines(filepath).each do |line|
          numbers = lineparser.parse_file_line(line)
          sorter.insert_new_data(numbers)
        end

        puts "Result numbers: #{sorter.highest_numbers}"
      rescue Exception => e
        puts "Problems with reading file #{filepath}. Error: #{e.message}"
      end
    end
  end
end