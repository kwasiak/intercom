module FileParser
  class FileLineParser

    def parse_file_line(file_line)

      # initial assertion
      raise "file_line cannot be nil" if file_line.nil?
      raise "file_line must be string" unless file_line.kind_of?(String)
      return [] if file_line == ''

      output=[]

      file_line.split(NUMBERS_LINE_SEPARATOR).each do |item|
        begin
          output << Float(item)
        rescue
          raise "'#{item}' is not a number"
        end
      end

      return output
    end

    private

    # separator of numbers in single line
    NUMBERS_LINE_SEPARATOR = '|'

  end
end